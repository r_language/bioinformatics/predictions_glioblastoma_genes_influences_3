In this R file we created multiple classifiers with glioblastoma data (gene influences) using inftum gbm for train data (540 genes, 1612 patient samples) :

- Pamr

- SVM (Support Vector Machine)

- Random Forest

- NN (Neural Network)

We also used those classifiers for predictions on test data (inftum recur db with 539 genes and 137 patient samples). We stored results in excel files for each classifier. Have fun !

Author : Marion Estoup 

Mail : marion_110@hotmail.fr 

Date : September 2023

